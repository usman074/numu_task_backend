const Sequelize = require('sequelize');
const User = require('../models/user')(sequelize, Sequelize.DataTypes);
const md5 = require('md5');

exports.signUp = async (request) => {
    try {

        const hashedPassword = md5(request.password);
        const user = await User.create({
            email: request.email,
            password: hashedPassword
        })
        return user;
    } catch (error) {
        throw new Error(error);
    }
}

exports.login = async (request) => {
    try {
        const user = await User.findOne({ where: { email: request.email } });
        return user;
    } catch (error) {
        throw new Error(error);
    }
}

