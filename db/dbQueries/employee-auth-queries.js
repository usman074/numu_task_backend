const Sequelize = require('sequelize');
const Employee = require('../models/employee')(sequelize, Sequelize.DataTypes);
const md5 = require('md5');

exports.signUp = async (request) => {
    try {
        const hashedPassword = md5(request.password);
        const employee = await Employee.create({
            email: request.email,
            password: hashedPassword
        })
        return employee;
    } catch (error) {
        throw new Error(error);
    }
}

exports.login = async (request) => {
    try {
        const employee = await Employee.findOne({ where: { email: request.email } });
        return employee;
    } catch (error) {
        throw new Error(error);
    }
}

