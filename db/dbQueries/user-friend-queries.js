const Sequelize = require('sequelize');

const Friend = require('../models/friend')(sequelize, Sequelize.DataTypes);

exports.sendFriendRequest = async (request, user) => {
    try {
        const checkReqestResponse = await checkRequest(user.id, request.receiverId);
        if (checkReqestResponse === null) {
            const friendRequest = await Friend.create({
                senderId: user.id,
                receiverId: request.receiverId
            })
            return friendRequest;
        } else {
            throw new Error("Already Received Friend Request From the user");
        }

    } catch (error) {
        throw new Error(error);
    }
}

exports.acceptFriendRequest = async (request, user) => {
    try {
        const friendRequest = await Friend.update({
            status: 1
        }, { where: { senderId: request.senderId, receiverId: user.id } });
        return friendRequest;
    } catch (error) {
        throw new Error(error);
    }
}

exports.deleteFriendRequestByReceiver = async (request, user) => {
    try {
        const friendRequest = await Friend.destroy(
            { where: { senderId: request.senderId, receiverId: user.id } });
        return friendRequest;
    } catch (error) {
        throw new Error(error);
    }
}

exports.deleteFriendRequestBySender = async (request, user) => {
    try {
        const friendRequest = await Friend.destroy(
            { where: { senderId: user.id, receiverId: request.receiverId } });
        return friendRequest;
    } catch (error) {
        throw new Error(error);
    }
}

exports.getAllFriendRequests = async (user) => {
    try {
        const friendRequest = await Friend.findAll({
            where: {
                senderId: user.id,
                status: 0
            }
        })
        return friendRequest;
    } catch (error) {
        throw new Error(error);
    }
}

async function checkRequest(senderId, receiverId) {
    const response = await Friend.findOne({ where: { senderId: receiverId, receiverId: senderId } });
    return response;
}

