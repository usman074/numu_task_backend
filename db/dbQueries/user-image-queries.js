const Sequelize = require('sequelize');
const fs = require('fs');
const Image = require('../models/image')(sequelize, Sequelize.DataTypes);

exports.uploadUserImage = async (filePath, user) => {
    try {
        const imageUpload = await Image.create({
            userId: user.id,
            imageUrl: filePath
        })
        return imageUpload;

    } catch (error) {
        throw new Error(error);
    }
}

exports.userImageApproval = async (req) => {
    try {
        if (req.approved) {
            const imageApproval = await Image.update({
                approved: req.approved
            }, { where: { id: req.imageId, userId: req.userId } });
            return imageApproval;
        } else {
            const image = await Image.findOne({
                where: { id: req.imageId, userId: req.userId }
            })
            fs.unlink(image.imageUrl, async function (err) {
                if (err) {
                    throw new Error(err);
                } else {
                    const imageUnapproved = await Image.destroy(
                        { where: { id: req.imageId, userId: req.userId } });
                    return imageUnapproved;

                }
            });
        }
    } catch (error) {
        throw new Error(error);
    }
}

exports.shareImage = async (req, user) => {
    try {
        const image = await Image.findOne({
            where: { id: req.imageId, userId: user.id }
        });

        if (image.approved) {
            const imageShare = await Image.update({
                share: 1
            }, { where: { id: req.imageId, userId: user.id } });
            return imageShare;
        } else {
            throw new Error("Cant share image before approval");
        }
    } catch (error) {
        throw new Error(error);
    }
}
