// const Sequelize = require('sequelize');
require('../conn');
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('user', {
        email: {
            type: DataTypes.STRING(100),
            allowNull: false,
            unique: true
        },
        role: {
            type: DataTypes.STRING(10),
            allowNull: false,
            defaultValue: "user"
        },
        password: {
            type: DataTypes.STRING(100),
            allowNull: false,
        }
    });

    User.associate = models => {
        User.hasMany(models.Friend,{
            foreignKey: "senderId",
            sourceKey: "id"
        });

        User.hasMany(models.Friend,{
            foreignKey: "receiverId",
            sourceKey: "id"
        });

        User.hasMany(models.Image, {
            foreignKey: "userId",
            sourceKey: "id"
        })
    }
    return User;
}
