// const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    var employee = sequelize.define('employee', {
        email: {
            type: DataTypes.STRING(100),
            allowNull: false,
            unique: true
        },
        role: {
            type: DataTypes.STRING(10),
            allowNull: false,
            defaultValue: "employee"
        },
        password: {
            type: DataTypes.STRING(100),
            allowNull: false,
        }
    });

    return employee;
}
