// const Sequelize = require('sequelize');
require('../conn');

module.exports = (sequelize, DataTypes) => {
    var Friend = sequelize.define('friends', {
        
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        senderId: DataTypes.INTEGER,
        receiverId: DataTypes.INTEGER
    }, {
        indexes: [
            {
                unique: true,
                fields: ['senderId', 'receiverId']
            }
        ]
    });

    Friend.associate = (models) => {
        Friend.belongsToMany(models.User, {
            as: 'senderId',
            foreignKey: 'senderId',
            targetKey: 'id'
        });

        Friend.belongsToMany(models.User, {
            as: 'receiverId',
            foreignKey: 'receiverId',
            targetKey: 'id'
        });
    }

    return Friend;
}
