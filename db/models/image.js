// const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    var Image = sequelize.define('image', {
        imageUrl: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        share: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        approved: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        userId: DataTypes.INTEGER
    });

    Image.associate = (models) => {
        Friend.belongsTo(models.User, {
            as: 'userId',
            foreignKey: 'userId',
            targetKey: 'id'
        });

    }

    return Image;
}
 