const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQueries/user-image-queries');
const auth_check = require('../Authorization/acl-policy');
const socketRequests = require('../socket/socket-requests');
const multer = require ('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb)=> {
        cb(null, 'uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
    }
})

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

router.post('/upload', auth_check.isAllowed , upload.single('userImage'), async (req, res) => {
        try {
            const response = await dbQuery.uploadUserImage(req.file.path, req.user);
            socketRequests.imageUploadNotification(req.user, 'Uploaded Image')
            res.json({
                user: response,
                success: true,
                error: null
            })

        } catch(error) {
            res.json({
                user: null,
                success: false,
                error: error.message
            })
        }
});

router.put('/share', auth_check.isAllowed , async (req, res) => {
    try {
        const response = await dbQuery.shareImage(req.body, req.user);
        res.json({
            user: response,
            success: true,
            error: null
        })

    } catch(error) {
        res.json({
            user: null,
            success: false,
            error: error.message
        })
    }
})

router.put('/approval', auth_check.isAllowed, async (req, res) => {
    try {
        const response = await dbQuery.userImageApproval(req.body);
        if (response) {
            res.json({
                user: response,
                success: true,
                error: null
            })
            socketRequests.imageApprovalNotification(req.body, "is approved");
        } else {
            socketRequests.imageApprovalNotification(req.body, "is unapproved");
            res.json({
                user: null,
                success: true,
                error: null
            })
        }
        

    } catch(error) {
        res.json({
            user: null,
            success: false,
            error: error.message
        })
    }
})

module.exports = router;
