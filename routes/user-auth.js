const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQueries/user-auth-queries');
const jwt = require('../Authorization/auth-jwt');
const md5 = require('md5');
router.post('/signup', async (req, res) => {
    try {
        const response = await dbQuery.signUp(req.body);
        response.password = undefined;
        res.json({
            user: response,
            token: null,
            success: true,
            error: null
        })
    } catch (error) {
        res.json({
            user: null,
            token: null,
            success: false,
            error: error.message
        })
    }
});

router.post('/login', async (req, res) => {
    try {
        const response = await dbQuery.login(req.body);

        if (response === null) {
            throw new Error("Incorrect Email")
        } else {
            if (md5(req.body.password) === response.password) {
                response.password = undefined;
                var token = jwt.sign(response);
                res.json({
                    user: response,
                    token: token,
                    success: true,
                    error: null
                })
            } else {
                throw new Error("Incorrect Password")
            }
        }

    } catch (error) {
        res.json({
            user: null,
            token: null,
            success: false,
            error: error.message
        })
    }
});

module.exports = router;