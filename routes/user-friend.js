const express = require('express');
const router = express.Router();
const dbQuery = require('../db/dbQueries/user-friend-queries');
const auth_check = require('../Authorization/acl-policy');
const socketRequests = require('../socket/socket-requests');

router.post('/sendRequest', auth_check.isAllowed , async (req, res) => {
    try {
        if (req.body.receiverId === req.user.id) {
            throw new Error("Cant send request");
        } else {
            const response = await dbQuery.sendFriendRequest(req.body, req.user);
            socketRequests.friendRequestNotification(req.body.receiverId, req.user, "send you friend request");
            res.json({
                user: response,
                success: true,
                error: null
            })
        }
    } catch (error) {
        res.json({
            user: null,
            success: false,
            error: error.message
        })
    }
});

router.put('/acceptRequest', auth_check.isAllowed , async (req, res) => {
    try {
            const response = await dbQuery.acceptFriendRequest(req.body, req.user);
            if (response[0]) {
            socketRequests.friendRequestNotification(req.body.senderId, req.user, "accepted your friend request");
                res.json({
                    user: response[0],
                    success: true,
                    error: null
                });
            } else {
                throw new Error("Could not accept friend request");
            }
    } catch (error) {
        res.json({
            user: null,
            success: false,
            error: error.message
        });
    }
});

router.delete('/receiver/deleteRequest', auth_check.isAllowed , async (req, res) => {
    try {
        const response = await dbQuery.deleteFriendRequestByReceiver(req.body, req.user);
        if (response) {
            res.json({
                user: response,
                success: true,
                error: null
            })
        } else {
            throw new Error("Could not delete friend request");
        }
    } catch(error) {
        res.json({
            user: null,
            success: false,
            error: error.message
        })
    }
});

router.delete('/sender/deleteRequest', auth_check.isAllowed , async (req, res) => {
    try {
        const response = await dbQuery.deleteFriendRequestBySender(req.body, req.user);
        if (response) {
            res.json({
                user: response,
                success: true,
                error: null
            })
        } else {
            throw new Error("Could not delete friend request");
        }
    } catch(error) {
        res.json({
            user: null,
            success: false,
            error: error.message
        })
    }
});

router.get('/getRequests', auth_check.isAllowed , async (req, res) => {
    try {
    const response = await dbQuery.getAllFriendRequests(req.user);
    res.json({
        user: response,
        success: true,
        error: null
    })
    } catch (error) {
        res.json({
            user: null,
            success: false,
            error: error.message
        })
    }
})

module.exports = router;