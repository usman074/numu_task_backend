var socketUser = [];
var socketEmployee = [];

exports.addSocketUser = (userId, socketId) => {
    socketUser[userId] = socketId;
}

exports.addSocketEmployee = (employeeId, socketId) => {
    socketEmployee[employeeId] = socketId;
}

exports.getSocketUser = () =>{
    return socketUser;
}

exports.getSocketEmployee = () => {
    return socketEmployee;
}