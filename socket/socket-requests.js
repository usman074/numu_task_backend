let io;
const socketConnectedUsers = require('./socket-connected-users');
exports.setSocketInstance = (socketInstance) => {
    io = socketInstance;
}

exports.friendRequestNotification = (receiver, sender, message) => {
    const socketUsers = socketConnectedUsers.getSocketUser();
    let socketId = socketUsers[receiver];
    io.to(socketId).emit('friend_request', sender.email+" "+message);
}

exports.imageUploadNotification = (sender, message) => {
    const socketEmployees = socketConnectedUsers.getSocketEmployee();
    socketEmployees.forEach(employee => {
        io.to(employee).emit('image_upload', sender.email+" "+message);
    })
}

exports.imageApprovalNotification = (receiver, message) => {
    const socketUsers = socketConnectedUsers.getSocketUser();
    let socketId = socketUsers[receiver.userId];
    io.to(socketId).emit('image_approval', `Image of id ${receiver.imageId} ${message}`);
}