'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addIndex(
      'friends', // name of Source model
      ['senderId', 'receiverId'],
      {
        unique: true
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeIndex('friends',
      ['senderId', 'receiverId'],
      { unique: true })
  }
};
