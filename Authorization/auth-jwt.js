var jwt = require('jsonwebtoken');
var secret = 'secretSignatutre'
var sign = function (user) {
    var token = jwt.sign({ exp: Math.floor(Date.now() / 1000) + (3600), user }, secret);
    return token;
}
var verify = function (token) {
    return new Promise(function (resolve, reject) {
        if (token == "null") {
            resolve({
                role: 'default'
            })
        }
        else {
            jwt.verify(token, secret, function (error, res) {
                if (error) {
                    reject(new Error(error.message));
                }
                else {
                    resolve(
                        res.user
                    )
                }
            });
        }
    });
}

module.exports = {
    sign: sign,
    verify: verify,
};