'use strict';

var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());


  let allow = acl.allow([{
    roles: ['user'],
    allows: [{
      resources: ['/user/friend', '/user/image'],
      permissions: '*'
    }]
  },
  {
    roles: ['employee'],
    allows: [{
      resources: ['/employee/image'],
      permissions: '*'
    }]
  }]);


exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.role : ['guest'];

  allow.then(() => {
    acl.addUserRoles('user','user', (err)=>{
    });
    acl.addUserRoles('employee','employee', (err)=>{
    });
    return Promise.resolve()
  }) .then(() => {
       acl.allowedPermissions(roles, [req.baseUrl], function(err, permissions){
})
acl.isAllowed(roles, req.baseUrl, [req.method.toLowerCase()], function (err, isAllowed) {

    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
  })
}
