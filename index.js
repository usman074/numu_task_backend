const express = require('express');
const bodyParser = require('body-parser');
const userAuth = require('./routes/user-auth');
const userFriend = require('./routes/user-friend');
const employeeAuth = require('./routes/employee-auth');
const userImage = require('./routes/user-image');
const jwt = require('./Authorization/auth-jwt');
const cors = require('cors')
const app = express();
const http = require('http').createServer(app);
const socketRequests = require('./socket/socket-requests');
const socketConnectedUsers = require('./socket/socket-connected-users');
app.use(cors({ origin: '*' }));
require('./db/conn');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/uploads', express.static('uploads'));

app.use((req, res, next) => {
    const token = req.headers["x-access-token"];
    jwt.verify(token).then((response) => {
        req.user = response;
        next();
    }).catch((error) => {
        return res.status(400).json({ "error": error.message });
    });
})


app.use('/user/auth', userAuth);
app.use('/user/friend', userFriend);
app.use('/user/image', userImage);
app.use('/employee/auth', employeeAuth);
app.use('/employee/image', userImage);

const io = require('socket.io')(http);

io.use(function (socket, next) {
    if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(socket.handshake.query.token).then((response) => {
            if (response.role === 'default') {
                next(new Error('Authentication error'));
            } else {
                socket.user = response;
                next();
            }
        }).catch((error) => {
            next(new Error('Authentication error'));
        });
    } else {
        next(new Error('Authentication error'));
    }
});

io.on('connection', (socket) => {
    console.log("user connected ", socket.id);
    console.log("user connected ", socket.user);
    if (socket.user.role === 'user') {
        socketConnectedUsers.addSocketUser(socket.user.id, socket.id);
    } else if (socket.user.role === 'employee') {
        socketConnectedUsers.addSocketEmployee(socket.user.id, socket.id);
    }
    socketRequests.setSocketInstance(io);
})


http.listen(3000, () => {
    console.log("Server is listening on port 3000");
})